import type {Config, Context} from '@netlify/functions'
import {handleRequest, writeNothingResponse} from '../gateway.mts'
import * as db from '../database.mts'

const createNotFoundResponse = () => new Response(undefined, {status: 404})

const retrieveKey = async (id: string) => {
  const value = await db.retrieveKey(id)
  if (value === undefined) {
    return createNotFoundResponse()
  }

  return new Response(value, {headers: {'Content-Type': 'text/plain'}})
}

const updateKey = async (id: string, request: Request) => {
  const value = await request.text()
  const result = await db.updateKey(id, value)
  if (result) {
    return new Response('Updated')
  }

  return createNotFoundResponse()
}

const deleteKey = async (id: string) => {
  const result = await db.deleteKey(id)
  if (result) {
    return new Response(null, {status: 204})
  }

  return createNotFoundResponse()
}

export default async function main(request: Request, context: Context) {
  const {id} = context.params
  const method = request.method
  switch (method) {
    case 'GET': {
      return handleRequest(request, async () => retrieveKey(id))
    }

    case 'PUT': {
      return handleRequest(request, async () => updateKey(id, request))
    }

    case 'DELETE': {
      return handleRequest(request, async () => deleteKey(id))
    }

    default: {
      return writeNothingResponse(request)
    }
  }
}

export const config: Config = {
  path: '/kvs/:id',
}
