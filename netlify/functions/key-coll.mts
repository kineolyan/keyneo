import type {Config, Context} from '@netlify/functions'
import {saveInDatabase} from '../database.mts'
import {handleRequest, writeNothingResponse} from '../gateway.mts'

const createKey = async (request: Request): Promise<Response> => {
  const value = await request.text()
  for (let i = 0; i < 5; i += 1) {
    // eslint-disable-next-line no-await-in-loop
    const key = await saveInDatabase(value)
    if (key !== null) {
      return new Response(key, {status: 201})
    }
  }

  return new Response('Failed to generate a key for the value', {status: 503})
}

export default async function main(request: Request, context: Context) {
  const method = request.method
  switch (method) {
    case 'POST': {
      return handleRequest(request, async () => createKey(request))
    }

    default: {
      return writeNothingResponse(request)
    }
  }
}

export const config: Config = {
  path: '/kvs',
}
