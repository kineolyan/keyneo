import type {Config, Context} from '@netlify/functions'
import * as db from '../database.mts'
import {handleRequest, writeNothingResponse} from '../gateway.mts'

const createKey = async (request: Request, context: Context): Promise<Response> => {
    const value = await request.text()
    const {id, code} = await db.createShare(value)
    return new Response(`${context.site.url}/share/${id}?code=${code}`)
}

export default async function main(request: Request, context: Context) {
    const method = request.method
    switch (method) {
        case 'POST': {
            return handleRequest(request, async () => createKey(request, context))
        }

    default: {
        return writeNothingResponse(request)
    }
    }
}

export const config: Config = {
    path: '/share',
}