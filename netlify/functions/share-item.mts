import type {Config, Context} from '@netlify/functions'
import {handleRequest, writeNothingResponse} from '../gateway.mts'
import * as db from '../database.mts'

const createNotFoundResponse = () => new Response(undefined, {status: 404})

const retrieveShare = async (id: string, code: string) => {
    if (id && code) {
        const value = await db.retrieveShare(id, code)
        if (value === undefined) {
            return createNotFoundResponse()
        }

        return new Response(value, {headers: {'Content-Type': 'text/plain'}})
    } else {
        return createNotFoundResponse()
    }
}

export default async function main(request: Requjest, context: Context) {
    const {id} = context.params
    const method = request.method
    const code = new URL(request.url).searchParams.get("code")
    switch (method) {
        case 'GET': {
            return handleRequest(request, async () => retrieveShare(id, code), {secure: false})
        }

    default: {
        return writeNothingResponse(request)
    }
    }
}

export const config: Config = {
    path: '/share/:id',
}
