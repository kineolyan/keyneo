const securityHeader = 'X-Skineo-Secret'
const envToken = 'KEYS_ACCESS_TOKEN'

const validate = (request: Request) => {
  const header = request.headers.get(securityHeader)
  const token = Netlify.env.get(envToken)
  return header === token
}

export const writeNoAccessResponse = () => new Response(undefined, {status: 403})

export const writeNothingResponse = (request: Request) =>
  new Response(`Called via ${request.method}`, {status: 404})

export const handleRequest = async (
  request: Request,
  f: () => Promise<Response>,
  options = {secure: true}
): Promise<Response> => {
  const accepted = validate(request)
  if (!options.secure || accepted) {
    try {
      return f()
    } catch (error) {
      console.error(error)
      return new Response(undefined, {status: 500})
    }
  }

  return writeNoAccessResponse()
}
