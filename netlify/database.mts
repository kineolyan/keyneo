import {randomUUID} from 'node:crypto'
import {Client, fql} from 'fauna'
import {encrypt, decrypt, envConfig as config} from './cypher.mts'

let instance = null
const client = () => {
  if (instance === null) {
    instance = new Client({
      secret: Netlify.env.get('FAUNADB_KEY'),
    })
  }

  return instance
}

export const saveInDatabase = async (value: string): Promise<string | undefined> => {
  const entry = {value: encrypt({config, data: value})}
  const query = fql` keyneo.create(${entry}) { id }`
  const response = await client().query(query)
  return response.data.id as string
}

export const retrieveKey = async (
  id: string | undefined,
): Promise<string | undefined> => {
  const query = fql`keyneo.byId(${id}) { value }`
  const response = await client().query(query)
  const value = response.data?.value
  console.log('read', value)
  return value === undefined ? undefined : decrypt({config, data: value})
}

export const updateKey = async (id: string, value: string): Promise<boolean> => {
  const entry = {value: encrypt({config, data: value})}
  const query = fql`keyneo.byId(${id})?.update(${entry})`
  const result = await client().query(query)
  return result.data !== null
}

export const deleteKey = async (id: string) => {
  const query = fql`keyneo.byId(${id})?.delete()`
  const result = await client().query(query)
  return result.data !== null
}

export const createShare = async(value: string) => {
  const code = randomUUID()
  const entry = {
    value: encrypt({config, data: value}),
    code,
  }
  const query = fql`shares.create(${entry}) { id, code }`
  const response = await client().query(query)
  return {
    id: response.data.id as string,
    code: response.data.code as string,
  }
}

export const retrieveShare = async (id: string, code: string): Promise<string|undefined> => {
  console.log(id, code)
  const query = fql`
  let entry = shares.all().where(.id == ${id} && .code == ${code}).first()
  let value = entry?.value
  entry?.delete()
  value
  `
  const {data: value} = await client().query(query)
  return value ? decrypt({data: value, config}) : undefined
}


