import {Buffer} from 'node:buffer'
import {createCipheriv, createDecipheriv, randomBytes} from 'node:crypto'

const secureTagLength = 16
export type SecureConfig = Readonly<{
  aad: string
  iv: Buffer
  key: string
}>
export const envConfig: SecureConfig = {
  // To generate the AAD, do `randomBytes(5).toString('base64')`
  aad: Buffer.from(Netlify.env.get('KEYS_ENCRYPTION_AAD'), 'base64'),
  // To generate the key, do `randomBytes(24).toString('base64')`
  key: Buffer.from(Netlify.env.get('KEYS_ENCRYPTION_KEY'), 'base64'),
}
export const testConfig = {
  aad: Buffer.from('0123456789', 'hex'),
  key: 'keykeykeykeykeykeykeykey',
}

export type Payload = Readonly<{
  config: SecureConfig
  data: string
}>

export type SecureData = Readonly<{data: string; nonce: string; tag: string}>
export const encrypt = ({
  data,
  config: {key, aad},
}: Readonly<{data: string; config: SecureConfig}>): SecureData => {
  const nonce = randomBytes(12)
  const cipher = createCipheriv('aes-192-ccm', key, nonce, {
    authTagLength: secureTagLength,
  })
  cipher.setAAD(aad, {
    plaintextLength: Buffer.byteLength(data),
  })
  const ciphertext = cipher.update(data, 'utf8')
  cipher.final()
  const tag = cipher.getAuthTag()
  const result = {
    data: ciphertext.toString('base64'),
    nonce: nonce.toString('base64'),
    tag: tag.toString('base64'),
  }
  console.log(result)
  return result
}

export const decrypt = ({
  data: {data, nonce, tag},
  config: {key, aad},
}: Readonly<{data: SecureData; config: SecureConfig}>): string => {
  const ciphertext = Buffer.from(data, 'base64')
  const decipher = createDecipheriv(
    'aes-192-ccm',
    key,
    Buffer.from(nonce, 'base64'),
    {
      authTagLength: secureTagLength,
    },
  )
  decipher.setAuthTag(Buffer.from(tag, 'base64'))
  decipher.setAAD(aad, {
    plaintextLength: ciphertext.length,
  })
  const receivedPlaintext = decipher.update(ciphertext, null, 'utf8')

  try {
    decipher.final()
  } catch (error) {
    throw new Error('Authentication failed!', {cause: error})
  }

  return receivedPlaintext
}
