Skineo
====

Set of lambda functions for many things. Deployed on netlify.

[![Netlify Status](https://api.netlify.com/api/v1/badges/165f63cb-fc93-4464-96e3-f616d21d0856/deploy-status)](https://app.netlify.com/sites/skineo/deploys)

Key-value encrypted storage
----

Key-value lambda services to store secrets online

### Basic usage

#### Create a new key

POST /kvs with body = value -> key-id

#### Get a key

GET /kvs/<key>

#### Update an existing key

PUT /kvs/<key> with body = value

#### Delete a key

DELETE /kvs/<key>

Secret sharing
----

Key-value one-shot value for easy sharing with anyone

### Basic usage

#### Create a new entry

POST /share with body = value -> link to retrieve the value

#### Get the entry

GET `/share/<key>?code=<code>`

Authentication if needed
----

Pass the header `X-Keyneo-Secret`.
